package handler

import (
	"context"

	log "github.com/micro/micro/v3/service/logger"

	testsrv "testsrv/proto"
)

type Testsrv struct{}

// Call is a single request handler called via client.Call or the generated client code
func (e *Testsrv) Call(ctx context.Context, req *testsrv.Request, rsp *testsrv.Response) error {
	log.Info("Received Testsrv.Call request")
	rsp.Msg = "Hello " + req.Name
	return nil
}

// Stream is a server side stream handler called via client.Stream or the generated client code
func (e *Testsrv) Stream(ctx context.Context, req *testsrv.StreamingRequest, stream testsrv.Testsrv_StreamStream) error {
	log.Infof("Received Testsrv.Stream request with count: %d", req.Count)

	for i := 0; i < int(req.Count); i++ {
		log.Infof("Responding: %d", i)
		if err := stream.Send(&testsrv.StreamingResponse{
			Count: int64(i),
		}); err != nil {
			return err
		}
	}

	return nil
}

// PingPong is a bidirectional stream handler called via client.Stream or the generated client code
func (e *Testsrv) PingPong(ctx context.Context, stream testsrv.Testsrv_PingPongStream) error {
	for {
		req, err := stream.Recv()
		if err != nil {
			return err
		}
		log.Infof("Got ping %v", req.Stroke)
		if err := stream.Send(&testsrv.Pong{Stroke: req.Stroke}); err != nil {
			return err
		}
	}
}
